# [ttf.ru](https://ttf.ru) source codes

<br/>

### Run ttf.ru on localhost

    # vi /etc/systemd/system/ttf.ru.service

Insert code from ttf.ru.service

    # systemctl enable ttf.ru.service
    # systemctl start ttf.ru.service
    # systemctl status ttf.ru.service

http://localhost:4046
