---
layout: page
title: Ищу работу на DeepLearning проеке аналитиком, программистом, devops, deep learning инженером
permalink: /
---

# Ищу работу на DeepLearning проеке аналитиком, программистом, devops, deep learning инженером

Здравствуйте!

Ищу возможность поработать на проектах DataScience в облаках.

Готов работать с linux, kubernetes, hadoop, kafka etc. 

Готов программировать, администрировать, выполнять функции devops инженера.

Если будет нужен человек на такого рода проекты, напишите мне.

Спасибо!

<br/>

**Для контактов:**

email:

![Marley](/img/a3333333mail.gif "Marley")

<br/>

Более подробно <a href="//programmist.net">здесь</a>



<br/>
<br/>

<div align="center">
  <strong>tags:</strong> облака, google, aws, azure, digital ocean, kubernets, hadoop, kafka, bigdata, machine learning, deep learning
</div>